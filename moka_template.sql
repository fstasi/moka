-- phpMyAdmin SQL Dump
-- version 4.0.6deb1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 06, 2014 at 03:37 PM
-- Server version: 5.5.35-0ubuntu0.13.10.2
-- PHP Version: 5.5.3-1ubuntu2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `moka_template`
--

-- --------------------------------------------------------

--
-- Table structure for table `answer`
--

DROP TABLE IF EXISTS `answer`;
CREATE TABLE IF NOT EXISTS `answer` (
  `element_id` int(20) NOT NULL,
  `Q1` float NOT NULL,
  `Q2` float NOT NULL,
  `Q3` float NOT NULL,
  `Q4` float NOT NULL,
  `Q5` float NOT NULL,
  `Q6` float NOT NULL,
  `Q7` float NOT NULL,
  `Q8` float NOT NULL,
  `Q9` float NOT NULL,
  `Q10` float NOT NULL,
  `Q11` float NOT NULL,
  `Q12` float NOT NULL,
  `Q13` float NOT NULL,
  `Q14` float NOT NULL,
  `Q15` float NOT NULL,
  `Q16` float NOT NULL,
  `Q17` float NOT NULL,
  `Q18` float NOT NULL,
  `Q19` float NOT NULL,
  `Q20` float NOT NULL,
  PRIMARY KEY (`element_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_estonian_ci;

--
-- Dumping data for table `answer`
--

INSERT INTO `answer` (`element_id`, `Q1`, `Q2`, `Q3`, `Q4`, `Q5`, `Q6`, `Q7`, `Q8`, `Q9`, `Q10`, `Q11`, `Q12`, `Q13`, `Q14`, `Q15`, `Q16`, `Q17`, `Q18`, `Q19`, `Q20`) VALUES
(1, 1, 1, -1, -1, -1, -1, -1, -1, 1, -1, -1, 1, -1, -1, -1, -1, -1, 0, -1, 1),
(2, -1, 1, 1, -1, -1, 1, 1, 1, 1, 1, 1, 1, -1, -1, -1, -1, -1, -1, -1, -1),
(3, -1, -1, 1, -1, -1, 1, -1, -1, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1),
(4, -1, -1, 1, -1, -1, -1, 1, 1, 1, -1, -1, 1, -1, 1, -1, -1, -1, -1, -1, 0),
(5, -1, 1, -1, 1, 1, -1, 1, -1, -1, -1, -1, 1, -1, -1, -1, -1, -1, -1, -1, -1),
(6, -1, 1, -1, 1, -1, -1, 1, 1, -1, -1, -1, 1, -1, -1, -1, -1, -1, -1, 1, 1),
(7, -1, 1, -1, 1, -1, -1, 1, -1, -1, -1, -1, 1, -1, -1, -1, -1, -1, -1, -1, 1),
(8, -1, -1, 1, -1, -1, 1, -1, -1, 1, -1, 1, 1, -1, 1, -1, -1, -1, -1, -1, -1),
(9, -1, 1, -1, 1, 1, -1, 1, 1, -1, -1, 1, 1, -1, -1, -1, -1, -1, -1, -1, -1),
(10, -1, -1, 1, -1, -1, -1, -1, -1, 1, -1, -1, 1, -1, -1, -1, 1, -1, 0, -1, 1),
(11, 1, -1, 1, -1, -1, 1, -1, -1, 1, -1, -1, 1, -1, -1, -1, -1, -1, -1, -1, -1),
(12, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 1, 1, -1, -1, -1, -1, -1, -1, -1),
(13, -1, -1, -1, 1, -1, -1, -1, -1, -1, -1, -1, 1, -1, -1, -1, -1, -1, -1, -1, -1),
(14, -1, 1, -1, 1, 1, -1, 1, 1, -1, -1, -1, 1, -1, -1, -1, -1, -1, -1, 1, -1),
(15, -1, 1, 1, -1, -1, -1, 1, 1, 1, -1, -1, 1, -1, -1, -1, -1, -1, -1, -1, -1),
(16, -1, 1, 1, -1, -1, -1, -1, -1, 1, -1, -1, 1, -1, -1, -1, -1, 1, -1, -1, 1),
(17, -1, 1, -1, 1, 1, -1, -1, -1, -1, -1, -1, 1, 1, -1, -1, -1, -1, -1, -1, -1),
(18, -1, 1, 1, -1, -1, -1, 1, -1, 1, 1, -1, 1, -1, -1, -1, 1, -1, -1, -1, 0),
(19, -1, -1, -1, -1, -1, -1, 1, 1, 1, -1, -1, 1, -1, -1, -1, -1, -1, 1, 1, 1),
(20, -1, 1, 1, -1, -1, -1, -1, -1, 1, -1, -1, 1, -1, -1, 1, -1, -1, 1, 0, 1),
(21, -1, 1, 1, -1, -1, -1, -1, -1, 1, -1, 1, 1, -1, -1, -1, -1, -1, -1, -1, 0),
(22, -1, 0, -1, -1, -1, -1, -1, -1, 1, -1, -1, 1, -1, -1, -1, -1, -1, -1, -1, 1),
(23, -1, 1, 1, -1, -1, -1, 1, -1, 1, -1, -1, 1, -1, -1, -1, -1, -1, -1, -1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `element`
--

DROP TABLE IF EXISTS `element`;
CREATE TABLE IF NOT EXISTS `element` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` text COLLATE utf8_estonian_ci NOT NULL,
  `n_guesses` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_estonian_ci AUTO_INCREMENT=24 ;

--
-- Dumping data for table `element`
--

INSERT INTO `element` (`id`, `name`, `n_guesses`) VALUES
(1, 'The Departed', 0),
(2, 'The Matrix', 0),
(3, 'Avatar', 0),
(4, 'The Dark Knight', 0),
(5, 'Cars', 0),
(6, 'The Hangover', 0),
(7, 'American Pie', 0),
(8, 'Avengers', 0),
(9, 'Toy Story', 0),
(10, 'Pulp Fiction', 0),
(11, 'Inception', 0),
(12, 'Titanic', 0),
(13, 'Forrest Gump', 0),
(14, 'Shrek', 0),
(15, 'Mission Impossible I', 0),
(16, 'Fight Club', 0),
(17, 'Finding Nemo', 0),
(18, 'Kill Bill', 0),
(19, 'The Godfather', 0),
(20, 'The Untouchables', 0),
(21, 'Limitless', 0),
(22, 'American History X', 0),
(23, 'Fast&Furious: Tokio D', 0);

-- --------------------------------------------------------

--
-- Table structure for table `question`
--

DROP TABLE IF EXISTS `question`;
CREATE TABLE IF NOT EXISTS `question` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `text` text COLLATE utf8_estonian_ci NOT NULL,
  `global_effectiveness` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_estonian_ci AUTO_INCREMENT=21 ;

--
-- Dumping data for table `question`
--

INSERT INTO `question` (`id`, `text`, `global_effectiveness`) VALUES
(1, 'Does Leo Di Caprio act in this movie?', 0),
(2, 'Has this movie a medium lenght (1:30 - 2:00 h)?', 0),
(3, 'Is this movie an action movie?', 0),
(4, 'Is this movie a comedy movie?', 0),
(5, 'Is this movie a cartoon?', 0),
(6, 'Is this movie a sci-fi movie?', 0),
(7, 'Does this movie have a sequel?', 0),
(8, 'Is this movie part of trilogy?', 0),
(9, 'Is there someone using guns in this movie?', 0),
(10, 'Is there someone using swords in this movie?', 0),
(11, 'Is there someone with superpowers in this movie?', 0),
(12, 'Is this movie set on the earth?', 0),
(13, 'Is this movie mainly set in sea?', 0),
(14, 'Is this movie based on a comic book?', 0),
(15, 'Does Robert De Niro act in this movie?', 0),
(16, 'Is this a Tarantino''s movie?', 0),
(17, 'Does Brad Pitt act in this movie?', 0),
(18, 'Is this a mafia movie?', 0),
(19, 'Is one of the main characters fat?', 0),
(20, 'Does the dialogues contain lot of bad words or offensive language?', 0);

-- --------------------------------------------------------

--
-- Table structure for table `statistics`
--

DROP TABLE IF EXISTS `statistics`;
CREATE TABLE IF NOT EXISTS `statistics` (
  `Scope` int(11) NOT NULL AUTO_INCREMENT,
  `tot_guesses` int(10) unsigned NOT NULL,
  PRIMARY KEY (`Scope`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_estonian_ci AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
