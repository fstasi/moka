package com.moka;

import java.util.logging.Level;

import com.moka.db.DbManager;
import com.moka.db.MovieInfoExtractor;
import com.moka.user.TestEngine;
import com.moka.user.TestEngineFra;
import com.moka.user.UpdateDatabase;
import com.moka.user.UserEngine;
import com.moka.utils.Mlog;

public class Main {

	public static void main(String[] args) {
		
		
		String	dbUrl = null,
				dbName = null,
				username = null,
				password = null,
				port = "3000";
		
		//get the args for connecting to the db
		for(int i = 0; i < args.length ; i++){
			
			String substr = args[i].substring(0, 2);
			String value = args[i].substring(2);
			
			if(substr.equals("-a")) dbUrl = value;
			if(substr.equals("-d")) dbName = value;
			if(substr.equals("-u")) username = value;
			if(substr.equals("-p")) password = value;
			if(substr.equals("-P")) port = value;
			
		}
		
		//dbName = "moka_template";
		//create the dbManager
		
		
		MovieInfoExtractor mie = new MovieInfoExtractor(dbUrl+":"+port, "moka_maxfra", username, password);
		
		/*
		
		Mlog.setLevel(Level.OFF);
		DbManager dbManager = new DbManager(dbUrl+":"+port, dbName, username, password);
		
		
		//create the data Structure
		DataStructure dataStructure = new DataStructure(dbManager);
		
		GameInstance gi = new GameInstance(dataStructure);
	
		//UpdateDatabase updDb = new UpdateDatabase(dataStructure,dbManager);	
		
		//UserEngine user = new UserEngine(gi, dataStructure);
		
		TestEngine test = new TestEngine(gi, dataStructure);
		//TestEngineFra test = new TestEngineFra(gi, dataStructure);
		 
		*/


	}

}
