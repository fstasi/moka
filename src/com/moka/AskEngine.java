package com.moka;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.logging.Level;

import com.moka.db.DbManager;
import com.moka.interfaces.DataInterface.Question;
import com.moka.interfaces.GameInterface.ReportRow;
import com.moka.utils.Mlog;

public class AskEngine {

	private DataStructure ds;

	private float[][] answers;

	// array of the probabilities for this game instance
	private float[] e_probabilities;

	// array of the questions already asked 
	private ArrayList<Integer> askedQuestions = new ArrayList<Integer>();

	// array of the answers given 
	private ArrayList<Float> givenAnswers = new ArrayList<Float>();



	// distance of the answers
	private float[] answer_dist;

	int do_cleanup = 0;

	public AskEngine(DataStructure dataStructure) {


		//populate the internal structure
		this.ds = dataStructure;
		answers = ds.getAnswers();

		answer_dist = new float[ds.getNumElements()];

		//initialize the element probabilities / scores
		this.e_probabilities = new float[ds.getNumElements()];
		for(int i = 0; i< e_probabilities.length; i++){
			e_probabilities[i]=1;
		}

	}
	
	private float scale(final float valueIn, final float limitMin, final float limitMax) {
        
		float baseMin = 0f;
		float baseMax = 1f;
		
		float val;
		
			val = (valueIn - limitMin) / (1 - limitMin);
		
		return val;
    }

	private ArrayList<SelectionResult> selectElements(float[] e_probs){


		ArrayList<SelectionResult> selected_elements = new ArrayList<SelectionResult>();
		

		float lowerLimit = 0.7f;
		
		float upperLimit = 1;

		for (; lowerLimit >=0.5f; lowerLimit-=0.05f){
			//pick the elements considered as acceptable

			selected_elements.clear();

			for(int row = 0; row < ds.getNumElements(); row++){

				//if the element is minor then the threshold, skip it
				if( e_probs[row] < lowerLimit  )
					continue;
				
				//if( e_probs[row] > upperLimit) upperLimit = e_probs[row];
				
				//else count if it's the best
				selected_elements.add(new SelectionResult(row,e_probs[row]));

			}

			//stop when I have at least 2 elements
			if(selected_elements.size() >= 2) break;

		}
		
		
		//riscalo tutto
		/*for (SelectionResult sel_elem : selected_elements ){
			
			sel_elem.value = this.scale(sel_elem.value, lowerLimit-0.5f, upperLimit);
			
		}
		*/

		return selected_elements;

	}
	
	
	public int selectQuestion(){
		
		/* STEP 0 : controllo se ho un vincitore...se si fermo il gioco */
		if (giveAnswerN()>=0) return -1;
		
		/* STEP 1 : scelgo su chi eseguire la selezione
		scegliere bene il subset su chi effettuare la selezione garantisce una convergenza migliore	 */
	
		ArrayList<SelectionResult> selected_elements = this.selectElements(this.e_probabilities);
		
		
		SelectionResult selectedQ = selectQuestion_x(selected_elements,this.e_probabilities,0);
		
		//add the question to the set of the already asked ones (you should not ask the same Q again)
		this.askedQuestions.add(selectedQ.index);
		
		return selectedQ.index;
		
	}

	public SelectionResult selectQuestion_x(ArrayList<SelectionResult> selected_elements, float[] e_probs,int lookAhead){

		int best_keyword = -1;

		//il valore minimo accettato di spread. Se la domanda non raggiunge questo livello, la butto via
		float best_spread_factor = 0.2f;
		
		/* STEP 2 : per il set di nodi selezionati, cerco la key che li splitta meglio
			devo considerare 2 fattori:
				- il numero di elementi (che deve essere equilibrato tra i rami L e R)
				- quanto si distanziano i 2 rami: più si distanziano, meglio è  */


		//testo ogni keyword sul set degli elementi selezionati
		for(int keyword=0; keyword < ds.getNumQuestions(); keyword++){

			//if I already asked the question, skip it
			if(this.askedQuestions.contains(keyword)) continue;

			float keyword_sum = 0;

			//creo un array che conterrà i valori pesati, per gli elementi selezionati
			float[] weighted_elements = new float[selected_elements.size()];

			for(int element=0; element < selected_elements.size() ; element++){

				int elem_id = selected_elements.get(element).index;
				float elem_prob = selected_elements.get(element).value;

				//più sono vicino allo 0, maggiore è la probabilità di errore
				
				float weighted_keyword = this.answers[elem_id][keyword] ;
				weighted_elements[element] =  weighted_keyword;

				keyword_sum += weighted_keyword;


			}

			//calcolo il valore medio (sulla keyword) delle risposte degli elementi
			float avrg_v = keyword_sum / selected_elements.size();
			float spread_from_avrg = 0;

			//calcolo la distanza di ogni risposta dal valore medio
			for(int element=0; element < selected_elements.size() ; element++){
				
				int elem_id = selected_elements.get(element).index;
				float elem_prob = selected_elements.get(element).value;
				
				spread_from_avrg += Math.abs(weighted_elements[element]-avrg_v)  * elem_prob;

			}
			
			float lookAheadSpread = 0f;
			if(lookAhead>0 && spread_from_avrg > 0.5 && this.askedQuestions.size() < 5){
				//push the question to simulate it has been picked
				this.askedQuestions.add(keyword);
				
				//calculate the next spread
				float[] probs_y = e_probs.clone();
				float[] probs_n = e_probs.clone();
				float[] answer_dist_y = this.answer_dist.clone();
				float[] answer_dist_n = this.answer_dist.clone();
				
				answerQ_x(1f, answer_dist_y, probs_y);		
				answerQ_x(-1f, answer_dist_n, probs_n);		
				lookAhead--;
				SelectionResult lookAheadSpreadY = this.selectQuestion_x(selected_elements, probs_y,lookAhead);
				SelectionResult lookAheadSpreadN = this.selectQuestion_x(selected_elements, probs_n,lookAhead);
				
				if(lookAheadSpreadY.index<0 || lookAheadSpreadN.index<0 )
					lookAheadSpread =  0;
				else
					lookAheadSpread = (lookAheadSpreadY.value + lookAheadSpreadN.value) / 4;
				
				//pop the question
				this.askedQuestions.remove(this.askedQuestions.size()-1);
			}
			
			//aggiungo lo spread factor del look ahead
			spread_from_avrg = (spread_from_avrg + lookAheadSpread);

			if(spread_from_avrg >= best_spread_factor ){
				best_spread_factor = spread_from_avrg;
				best_keyword = keyword;
			}

		}		
		
		SelectionResult ret = new SelectionResult(best_keyword,best_spread_factor);

		return ret;
	}


	//riequilibra le probabilità
	public void probablities_cleanup(){

		//prende gli elementi più probabili (quelli con dist factor minore)
		int best_elem=-1;
		float min_dist=1*givenAnswers.size();
		int n_best = 0;
		for(int elem=0; elem<answer_dist.length; elem++){
			if(answer_dist[elem] < min_dist){
				best_elem=elem;

				//se il miglior elemento ha già probabilità 1, esco
				if(e_probabilities[best_elem]==1) return;

				min_dist = answer_dist[elem];
				n_best = 1;
			}
			if(answer_dist[elem] == min_dist) n_best++;
		}



		//se c'è + di 1 best...è complicato..meglio skippare al prossimo giro

		if(n_best>1)
			return;


		//cerca la peggior risposta per il miglior film
		int worst_answ = 0;
		float worst_answ_dist = 0;
		float correcting_answ = 0;
		for(int answ=0; answ < askedQuestions.size(); answ++){

			float expected = answers[best_elem][askedQuestions.get(answ)];

			float distance = Math.abs(expected - givenAnswers.get(answ))/2;

			if(distance >= worst_answ_dist){
				worst_answ = answ;
				worst_answ_dist = distance;
				correcting_answ = Math.round(expected);
			}

		}


		//dopo averla trovata, ricalcola la probabilità degli elementi
		for(int row=0; row< ds.getNumElements(); row++){

			float new_dist = 0;
			for(int answ=0; answ < askedQuestions.size(); answ++){

				//se è la domanda peggiore, la correggo
				float given = (answ == worst_answ) ? correcting_answ : givenAnswers.get(answ);

				float expected = answers[row][askedQuestions.get(answ)];
				float distance = Math.abs(expected - given)/2;

				new_dist += distance; 

			}
			this.answer_dist[row] = new_dist;
			this.e_probabilities[row] = 1 - (new_dist/this.askedQuestions.size());

		}

		Mlog.i("", String.format("Cleaning up: %s | %.3f ", ds.getElement(best_elem).name, e_probabilities[best_elem]));


		//azzero il contatore del cleanup (così che non venga rifatto al giro dopo)
		do_cleanup = 0;


	}
	
	public boolean answerQ(float response){
		
		if( response > DataStructure.MAX_RANGE || response < DataStructure.MIN_RANGE) return false;
		
		this.givenAnswers.add(response);
		
		float[] answ_dist = this.answer_dist;
		float[] e_probs = this.e_probabilities;
		
		return answerQ_x(response, answ_dist, e_probs);
		
	}

	public boolean answerQ_x(float response, float[] answ_dist, float[] e_probs){


		if( response > DataStructure.MAX_RANGE || response < DataStructure.MIN_RANGE) return false;


		int key = this.askedQuestions.get(this.askedQuestions.size()-1);

		//recalculate the probabilities

		String tag;

		//calculate the distance from the answer
		for(int row=0; row< ds.getNumElements(); row++){

			float expected = this.answers[row][key];

			//the smaller the distance, the more accurate the element
			float distance = Math.abs(expected - response)/2;
			answ_dist[row] += distance;

			//calculate the average distance of the element, and use it as a probability factor

			float new_probability = 1 - (answ_dist[row]/this.askedQuestions.size());


			if(new_probability < e_probs[row]) tag="v";
			else tag="^";

			e_probs[row] = new_probability;

			Mlog.i("", String.format("%s | %.3f | %3d - %s", tag, e_probabilities[row],row,ds.getElement(row).name ));

		}

		return true;


	}

	public int giveAnswerN(){

		if(this.askedQuestions.size() <= 3) return -1;

		float minDistance= 2 * this.askedQuestions.size();
		int maxI=-1;

		float distFactor = 1.2f ;

		//float error_prob = Math.round(this.askedQuestions.size()/10);
		//tolleriamo 1 errore grave ogni 10 domande = 1/10 = 0.1


		boolean stopAsking=false;
		for(int col=0; col< ds.getNumElements(); col++){

			//check if the element is the most probable
			if(this.answer_dist[col] < minDistance){

				//if it tops the previous max, give the answer
				if(this.answer_dist[col] + distFactor < minDistance){
					stopAsking = true;
					//Mlog.d("", ds.elementList.get(col).getName() +" doubles "+ maxI);
				}else stopAsking = false;

				minDistance=this.answer_dist[col];
				maxI=col;

			}else{
				if( stopAsking && minDistance + distFactor >= this.answer_dist[col]){
					stopAsking = false;
				}
			}
		}

		if(stopAsking)
			return maxI;
		
		return -1;
		

	}

	public int giveAnswer(){


		float maxV=-1f;
		int maxI=-1;

		if(this.askedQuestions.size() <= 1) return -1;

		float distFactor = 1 - (1.0f/ (this.askedQuestions.size()/2) );

		//tolleriamo 1 errore grave ogni 10 domande = 1/10 = 0.1


		boolean stopAsking=false;
		for(int col=0; col< ds.getNumElements(); col++){

			//check if the element is the most probable
			if(this.e_probabilities[col] >= maxV){

				//if it tops the previous max, give the answer
				if(this.e_probabilities[col]*distFactor > maxV){
					stopAsking = true;
					//Mlog.d("", ds.elementList.get(col).getName() +" doubles "+ maxI);
				}else stopAsking = false;

				maxV=this.e_probabilities[col];
				maxI=col;

			}else{
				if( stopAsking && maxV*distFactor <= this.e_probabilities[col]){
					stopAsking = false;
				}
			}
		}

		if(stopAsking){
			return maxI;
		}else{
			return -1;
		}


	}



	public List<ReportRow> getReport() {

		int best_elem = this.getBestElement();

		List<ReportRow> report = new ArrayList<ReportRow>();

		for(int i = 0 ; i < this.askedQuestions.size() ; i++ ){

			ReportRow r = new ReportRow();

			int question_id = this.askedQuestions.get(i);

			r.questionName = ds.getQuestion(question_id).text;
			r.answerGiven = this.givenAnswers.get(i);
			r.answerExpected = this.answers[best_elem][question_id];

			report.add(r);

		}

		return report;

	}

	
	private ArrayList<SelectionResult> getListLimits(float[] e_probs){
		
		int best_elem = -1, worst_elem = -1;
		float best_elem_v = 0;
		float worst_elem_v = 1;
		for(int i = 0; i<e_probs.length; i++){
			if(e_probs[i] > best_elem_v){
				best_elem_v = e_probs[i];
				best_elem = i;
			}
			
			if(e_probs[i] < worst_elem_v){
				worst_elem_v = e_probs[i];
				worst_elem = i;
			}
			
		}
		
		ArrayList<SelectionResult> ret = new ArrayList<SelectionResult>();
		ret.add( new SelectionResult(best_elem,best_elem_v) );
		ret.add( new SelectionResult(worst_elem,worst_elem_v) );
		
		return ret;
	}

	public int getBestElement() {

		return this.getListLimits(this.e_probabilities).get(0).index;
		
	}
	
	
	public class SelectionResult {
		
		public int index = 0;
		public float value = 0.0f;
		
		public SelectionResult(int index, float value) {
			this.index = index;
			this.value = value;
		}
		
	}


}
