package com.moka;

import com.moka.interfaces.GameInterface;

import java.util.ArrayList;
import java.util.List;

public class GameInstance implements GameInterface{
	
	
	// pointer to the base datastructure
	private DataStructure ds;
	
	//can be NORMAL, LEARNING, DEBUG, LEARNING_DEBUG
	private gameMode gameMode;
	
	
	private AskEngine engine;
	
	
	public GameInstance(DataStructure ds) {
		this.ds = ds;
	}
	
	
	/** Starts the game, resetting the current session. 
	 * Returns a boolean indicating if the game has started correctly (true) or a problem happened (false). 
	 * */
	@Override
	public boolean startGame(gameMode mode) {
		
		//restart the game data structure
	
		this.gameMode = mode;
		
		engine = null;
		engine = new AskEngine(ds);
		
		return true;
	}

	@Override
	public String getQuestion() {
		
		int selectedQ = engine.selectQuestion();
		
		if(selectedQ<0) return null;
		
		return ds.getQuestion(selectedQ).text;
	}

	@Override
	public boolean sendAnswerToQuestion(float answer) {
		return engine.answerQ(answer);
	}

	@Override
	public String endGame() {
		
		return ds.getElement(engine.getBestElement()).name;
	}

	@Override
	public List<ReportRow> getReport() {
		return engine.getReport();
	}

}
