package com.moka.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.moka.utils.Mlog;

/**
 * @author F71709A
 *
 */
public class MovieInfoExtractor {

	private PreparedStatement psUpdate;

	private Map<String, Integer> currencies = new HashMap<String,Integer>();
	private Map<Integer,MovieInfo> movieList = new HashMap<Integer,MovieInfo>();
	private Map<Integer,ActorInfo> actorsCounter = new HashMap<Integer,ActorInfo>();

	private int errors = 0;

	//connection attributes
	String dbUrl, dbName, username, password;
	
	// db manager to update moka db
	private DbManager dbm;
	
	public MovieInfoExtractor(String dbUrl,String dbName,String username,String password) {


		this.dbUrl = dbUrl;
		this.dbName = dbName;
		this.username = username;
		this.password = password;
		
		dbm = new DbManager(dbUrl, dbName, username, password);

		this.readTitles();
		
		this.readActors();
		//this.readGross();

		if(errors == 0){
			//saveStructure();
			//printStructure();
			//movieList.get(2590603).print();
			printActorCounter();

		}else{
			System.out.println("Error occurred. Database has not been modified.");
		}


	}

	// --- private methods ---

	private void readTitles(){

		String query = "SELECT mk.movie_id,mk.title,mk.countries,mk.production_year, iat.title as ita_title, iat.note"
					+ " FROM moka_test.moka_movie_backup as mk"
					+ " LEFT JOIN imdb.aka_title AS iat ON iat.movie_id = mk.movie_id"
					+ " WHERE iat.note LIKE '%italy%'";

		try
		{

			Connection conn = openDbConnection();
			Statement stmt = conn.createStatement();

			System.out.print("Extracting aka_titiles info from imdb...");
			ResultSet rs = stmt.executeQuery(query);

			while (rs.next())
			{

				//get the right element or push a new one
				MovieInfo mi = movieListPushItem(rs.getInt("movie_id"));

				//set the new title and production year
				if(mi.title == null) mi.title = rs.getString("title");
				if(mi.productionYear == 0) mi.productionYear = rs.getInt("production_year");
				if(mi.countries == null) mi.countries = rs.getString("countries");

				//add a new aka titile to the list of the aka titles of the element
				mi.addAkaTitle(rs.getString("note").toLowerCase(), rs.getString("ita_title"));

			}

			//chiude i recordset, lo statement e la connessione al db
			rs.close();
			stmt.close();
			conn.close();

			System.out.println("done!");

		}
		catch (SQLException e)
		{
			e.printStackTrace();
			System.exit(1);
		}

	}

	private void readGross(){

		String query = "SELECT title.id, title.title, title.production_year, info FROM imdb.`movie_info`, imdb.title where info_type_id = 107 and title.id = movie_info.movie_id order by title.id";

		String regex_pattern = "^([^\\d\\s]*)\\s*([0-9,\\.]*)\\s*\\(([.\\w\\-\\s]*)\\)\\s*.*" ;
		Pattern pattern = Pattern.compile(regex_pattern);
		Matcher matcher;

		//significato: inizio stringa > (nonNumero)* > (numeri punti e virgole) > spazio opzionale > (paese)

		try
		{

			Connection conn = openDbConnection();
			Statement stmt = conn.createStatement();

			System.out.print("Extracting gross info from imdb...");
			ResultSet rs = stmt.executeQuery(query);

			while (rs.next())
			{

				//get the right element or push a new one
				MovieInfo mi = movieListPushItem(rs.getInt("id"));

				//set the new title and production year
				if(mi.title == null) mi.title = rs.getString("title");
				if(mi.productionYear == 0) mi.productionYear = rs.getInt("production_year");


				String curInfo = rs.getString("info");

				matcher= pattern.matcher(curInfo);

				if (matcher.find()) {

					try{

						//da ogni riga estraggo currency, gross e paese
						String currency= matcher.group(1);
						Long gross = new Long(matcher.group(2).replaceAll("[\\D]", ""));
						String country = matcher.group(3);
						addCurrency(mi.addGross(currency, country, gross));

					}catch(Exception e){
						System.out.println("error on "+curInfo + " "+ e.toString());
					}

				}else {
					errors++;
				}

			}

			//chiude i recordset, lo statement e la connessione al db
			rs.close();
			stmt.close();
			conn.close();

			System.out.println("done!");


		}
		catch (SQLException e)
		{
			e.printStackTrace();
			System.exit(1);
		}

	}
	
	private void readActors(){
		
		int topActors = 4;
		String query = "SELECT DISTINCT na.id, na.name,ci.nr_order, mk.movie_id, mk.title_ita"
				+ " FROM imdb.name na"
				+ " INNER JOIN imdb.cast_info ci on ci.person_id = na.id"
				+ " INNER JOIN moka_test.moka_movie_backup mk on ci.movie_id = mk.movie_id"
				+ " WHERE mk.selected = 1"
				+ " AND ci.role_id IN (1,2) #1 = attori, 2=attrici, 8 = registi"
				+ " AND ( ci.note IS NULL OR ( ci.note NOT LIKE '%uncredited%' AND ci.note NOT LIKE '%voice%' ) )"
				+ " AND ci.nr_order IS NOT NULL"
				+ " AND ci.nr_order < "+topActors;

		try
		{

			Connection conn = openDbConnection();
			Statement stmt = conn.createStatement();

			System.out.print("Extracting actors info from imdb...");
			ResultSet rs = stmt.executeQuery(query);

			while (rs.next())
			{

				//get the right element or push a new one
				MovieInfo mi = movieListPushItem(rs.getInt("movie_id"));

				//add a new actor to the list
				ActorInfo ai = new ActorInfo();
				ai.id = mi.addActor(rs.getInt("id"), rs.getInt("nr_order"));
				ai.setName( rs.getString("name"));
				
				addActorCounter(ai);


			}

			//chiude i recordset, lo statement e la connessione al db
			rs.close();
			stmt.close();
			conn.close();

			System.out.println("done!");

		}
		catch (SQLException e)
		{
			e.printStackTrace();
			System.exit(1);
		}
			
		
	}

	/**
	 * Prints to console every currency found and the number of occurrencies for each one
	 */
	private void printCurrencies(){

		for (Map.Entry<String,Integer> entry : currencies.entrySet()) {
			System.out.println(String.format("%s,%d", entry.getKey(),entry.getValue() ));
		}

	}
	
	/**
	 * Prints to console every actor found and the number of occurrencies for each one
	 */
	private void printActorCounter(){

		for (Map.Entry<Integer,ActorInfo> entry : actorsCounter.entrySet()) {
			
			int lowPos = 0;
			
			if(entry.getValue().movieCounter > 20){
				//System.out.println(String.format("%d,%s,%d", entry.getKey(),entry.getValue().name,entry.getValue().movieCounter ));
				
				for (MovieInfo mi : movieList.values()) {
					
					if(mi.actors.containsKey(entry.getKey()))
						if (mi.actors.get(entry.getKey()) > 8){
							lowPos ++;
							System.out.println(String.format("\t%s %s in %s", entry.getValue().name, entry.getValue().surname, mi.title ));
						}

					
				}
				
			}
		}

	}

	/**
	 * Prints to console every element in the structure
	 */
	private void printStructure(){


		for (MovieInfo mi : movieList.values()) {
			mi.convertToDollar();
			mi.calculateItaTitle();

			mi.print();
		}

	}

	/**
	 * Updates the database setting the italian_title, the gross_usd and gross_eur for every element
	 */
	private void saveStructure(){

		Connection conn = null;

		try {
			conn = openDbConnection();
			conn.setAutoCommit(false);
			psUpdate = conn.prepareStatement("UPDATE moka_test.moka_movie_backup SET title_ita = ?, gross_usd = ?, gross_eur = ? WHERE movie_id = ?");
			int batchCounter = 0;
			for (MovieInfo mi : movieList.values()) {


				//do all the conversions
				mi.convertToDollar();
				mi.calculateItaTitle();

				psUpdate.setString(1,mi.titleIta);
				psUpdate.setLong(2,mi.maxGrossDollar);
				psUpdate.setLong(3,mi.maxGrossEuro);
				psUpdate.setLong(4,mi.id);
				//add the statement to a batch
				psUpdate.addBatch();

				if ((batchCounter + 1) % 1000 == 0) {// flush the batch every 1000 items.
					System.out.print("Flushing batch "+(batchCounter-999)+"-"+batchCounter+"...");
					psUpdate.executeBatch();
					System.out.println("done!");
				}
				batchCounter++; //increase the batch counter

			}
			// Execute the last batch
			System.out.print("Flushing last batch...");
			psUpdate.executeBatch();
			conn.commit();
			System.out.println("done!\n\nLunch is ready! Enjoy :) ");

		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			if (psUpdate != null) try { psUpdate.close(); } catch (SQLException e1) {}
			if (conn != null) try { conn.close(); } catch (SQLException e2) {}
		}

	}
	
	private void generateActorQuestions(){
		
		
		String question = "Does $ acts in this movie?";
		
		//hashmap used by the dbm
		HashMap<Integer,Float> movieAnswerPairs = new HashMap<Integer,Float>();
		
		//iterate over the actors -> every actor will become a question
		for (Map.Entry<Integer,ActorInfo> entry : actorsCounter.entrySet()) {
			
			//generate the question text
			String Q = question.replace("$", entry.getValue().name +" "+entry.getValue().surname);
			
			//add the question to moka db
			int questionId = dbm.addQuestion(Q);
			
			//for every actor (question) add 1 answer for every movie
			for (MovieInfo mi : movieList.values()) {
				
				int movieId = mi.id;
				float answer = -1;
				
				//if the actor acts in the movie, the answer is yes
				if(mi.actors.containsKey(entry.getKey()))
					answer = 1;
				
				movieAnswerPairs.put(movieId, answer);
				
			}
			
			//finally write the answers for the actor (question)
			dbm.setAnswers(questionId, movieAnswerPairs);
								
		}

	}

	/**
	 * @param itemId
	 * @return a new MovieInfo or the old MovieInfo associated to the given itemId
	 */
	private MovieInfo movieListPushItem(int itemId){
		if(movieList.containsKey(itemId)){
			return movieList.get(itemId);
		}else{
			MovieInfo mi = new MovieInfo();
			mi.id = itemId;
			movieList.put(itemId, mi);
			return mi;
		}
	}


	/**
	 * @param currency to be added to the list of currencies.
	 * <p>If currency already exists, increase the counter.</p>
	 */
	private void addCurrency(String currency){

		if(currency.length()<=0) return;

		if(currencies.containsKey(currency)){
			currencies.put(currency, currencies.get(currency) + 1);	
		}else{
			currencies.put(currency,1);
		}
	}
	
	
	private void addActorCounter(ActorInfo actor){
		
		if(actor.id<=0) return;
		
		if(actorsCounter.containsKey(actor.id)){
			
			ActorInfo ai = actorsCounter.get(actor.id);
			ai.movieCounter+=1;
			actorsCounter.put(actor.id, ai);	
			
		}else{
			actorsCounter.put(actor.id,actor);
		}
		
	}

	private Connection openDbConnection()
	{

		Connection co = null;
		try
		{
			System.out.print("Connecting to "+dbUrl+"...");
			Class.forName("com.mysql.jdbc.Driver");
			co = DriverManager.getConnection("jdbc:mysql://"+dbUrl+"/"+dbName+"?useServerPrepStmts=false&rewriteBatchedStatements=true&user="+username+"&password="+password+"");
			System.out.println("Success!");
		}
		catch (ClassNotFoundException e)
		{
			System.out.println("FAIL!");
			Mlog.e("DbManager", e.getMessage());
			System.exit(1);
		}
		catch (SQLException e)
		{
			System.out.println("FAIL!");
			Mlog.e("DbManager", e.getMessage());
			System.exit(1);
		}

		return co;
	}



	public class MovieInfo
	{
		public int id;
		public String title = null;
		public String titleIta = null;
		public String countries = null;
		public int productionYear = 0;
		private Map<String, Long> gross = new HashMap<String,Long>();
		private Map<String, String> akaTitles = new HashMap<String,String>();
		private Map<Integer,Integer> actors = new HashMap<Integer,Integer>();
		public Long maxGrossDollar = (long) 0;
		public Long maxGrossEuro = (long) 0;


		private int yearAttenuation = 10;

		public void print(){
			//info generiche
			System.out.println(String.format("(id: %s) %s (year: %d) - EuroGross: %d, DollarGross: %d", this.id, this.title, this.productionYear, this.maxGrossEuro, this.maxGrossDollar ));

			//info gross
			/*
			for (Map.Entry<String,Long> entry : gross.entrySet()) {
			    System.out.println(String.format("\t%-15s %s", entry.getKey(),entry.getValue() ));
			}
			 */

			//info titles
			/*for (Map.Entry<String,String> entry : akaTitles.entrySet()) {
				System.out.println(String.format("\t%-15s %s", entry.getKey(),entry.getValue() ));
			}
			*/
			
			//info actors
			for (Entry<Integer, Integer> entry : actors.entrySet()) {
				System.out.println(String.format("\t%d %s",entry.getValue(), actorsCounter.get(entry.getKey()).name ));
			}
			
			
		}
		
		
		public int addActor(Integer actorId, Integer position){
			
			//check if the actor is present already for this movie
			if(actors.containsKey(actorId)){
				
				//update the position, if the new one is better
				if ( position < actors.get(actorId))
					actors.put(actorId, position);
				
				return 0;
			}
				
			//if it's not present, add the new element and return it's ID (so it will globally counted)
			actors.put(actorId,position);
			
			return actorId;
		}

		/**
		 * @param currency
		 * @param country
		 * @param grossval
		 * @return the inner hash key in the format of currency|country
		 * <p>automatically holds only the largest grossval, for the currency|country association</p>
		 */
		public String addGross(String currency, String country, Long grossval) {

			if(gross.containsKey(currency+"|"+country)){
				if( gross.get(currency+"|"+country).compareTo(grossval) < 0 )
					gross.put(currency+"|"+country,grossval);
				return "";
			}else{
				gross.put(currency+"|"+country,grossval);
				return currency+"|"+country;
			}

		}

		public void addAkaTitle(String note, String title) {
			if(note!=null && title!=null)
				akaTitles.put(note,title);
		}


		/**
		 * iterates the akaTitles and set the best, accordingly to the priority, as default Italian title
		 */
		public void calculateItaTitle(){

			//if not even one akatitle is found, let's just use the original title
			if(akaTitles.size()==0 || (countries!=null && countries.equalsIgnoreCase("italy")) ){
				titleIta = title;
				return;
			}

			String[] title_priorities = new String[]{	
					"(italy)",
					"(italy) (imdb display title)",
					"(italy) (dvd title)"
			};

			for(int i = 0; i < title_priorities.length; i++ ){

				if(akaTitles.containsKey(title_priorities[i])){
					titleIta = akaTitles.get(title_priorities[i]);
					return;
				}

			}

			// if i cannot find any good title, i take the first one
			for (Map.Entry<String,String> entry : akaTitles.entrySet()) {
				titleIta = entry.getValue(); return;
			}




		}

		public void convertToDollar() {

			Long grossConverted;

			// DOLLAR ------
			if(gross.containsKey("$|USA")){
				grossConverted = gross.get("$|USA");
				grossConverted = adaptOnUsInflation(grossConverted,productionYear);
				if( grossConverted > maxGrossDollar ) maxGrossDollar = grossConverted;
			}
			if(gross.containsKey("£|UK"))
			{
				grossConverted = (long) (gross.get("£|UK")*1.7);
				grossConverted = adaptOnUkInflation(grossConverted,productionYear);
				if( grossConverted > maxGrossDollar ) maxGrossDollar = grossConverted;
			}



			// EURO ------
			if(gross.containsKey("€|Italy") && productionYear > 2000)
			{
				grossConverted = gross.get("€|Italy");
				grossConverted = adaptOnItalianInflation(grossConverted,productionYear);
				if( grossConverted > maxGrossEuro ) maxGrossEuro = grossConverted;
			}
			if(gross.containsKey("ITL|Italy")){
				grossConverted = gross.get("ITL|Italy")/1936;
				grossConverted = adaptOnItalianInflation(grossConverted,productionYear);
				if( grossConverted > maxGrossEuro ) maxGrossEuro = grossConverted;
			}



		}

		public long adaptOnItalianInflation(Long gross, int year) 
		{
			long grossConverted = 0;
			switch(year)
			{
			case 1920: grossConverted = (long) (gross*2186); break;
			case 1921: grossConverted = (long) (gross*1847); break;
			case 1922: grossConverted = (long) (gross*1859); break;
			case 1923: grossConverted = (long) (gross*1869); break;
			case 1924: grossConverted = (long) (gross*1806); break;
			case 1925: grossConverted = (long) (gross*1607); break;
			case 1926: grossConverted = (long) (gross*1490); break;
			case 1927: grossConverted = (long) (gross*1630); break;
			case 1928: grossConverted = (long) (gross*1759); break;
			case 1929: grossConverted = (long) (gross*1731); break;
			case 1930: grossConverted = (long) (gross*1788); break;
			case 1931: grossConverted = (long) (gross*1979); break;
			case 1932: grossConverted = (long) (gross*2032); break;
			case 1933: grossConverted = (long) (gross*2160); break;
			case 1934: grossConverted = (long) (gross*2277); break;
			case 1935: grossConverted = (long) (gross*2246); break;
			case 1936: grossConverted = (long) (gross*2088); break;
			case 1937: grossConverted = (long) (gross*1907); break;
			case 1938: grossConverted = (long) (gross*1771); break;
			case 1939: grossConverted = (long) (gross*1696); break;
			case 1940: grossConverted = (long) (gross*1454); break;
			case 1941: grossConverted = (long) (gross*1256); break;
			case 1942: grossConverted = (long) (gross*1087); break;
			case 1943: grossConverted = (long) (gross*648); break;
			case 1944: grossConverted = (long) (gross*146); break;
			case 1945: grossConverted = (long) (gross*74.0); break;
			case 1946: grossConverted = (long) (gross*62.7); break;
			case 1947: grossConverted = (long) (gross*38.7); break;
			case 1948: grossConverted = (long) (gross*36.5); break;
			case 1949: grossConverted = (long) (gross*36.0); break;
			case 1950: grossConverted = (long) (gross*36.5); break;
			case 1951: grossConverted = (long) (gross*33.3); break;
			case 1952: grossConverted = (long) (gross*31.9); break;
			case 1953: grossConverted = (long) (gross*31.3); break;
			case 1954: grossConverted = (long) (gross*30.5); break;
			case 1955: grossConverted = (long) (gross*29.6); break;
			case 1956: grossConverted = (long) (gross*28.2); break;
			case 1957: grossConverted = (long) (gross*27.7); break;
			case 1958: grossConverted = (long) (gross*26.4); break;
			case 1959: grossConverted = (long) (gross*26.5); break;
			case 1960: grossConverted = (long) (gross*25.8); break;
			case 1961: grossConverted = (long) (gross*25.2); break;
			case 1962: grossConverted = (long) (gross*23.9); break;
			case 1963: grossConverted = (long) (gross*22.3); break;
			case 1964: grossConverted = (long) (gross*21.0); break;
			case 1965: grossConverted = (long) (gross*20.1); break;
			case 1966: grossConverted = (long) (gross*19.7); break;
			case 1967: grossConverted = (long) (gross*19.4); break;
			case 1968: grossConverted = (long) (gross*19.1); break;
			case 1969: grossConverted = (long) (gross*18.6); break;
			case 1970: grossConverted = (long) (gross*17.7); break;
			case 1971: grossConverted = (long) (gross*16.8); break;
			case 1972: grossConverted = (long) (gross*15.9); break;
			case 1973: grossConverted = (long) (gross*14.4); break;
			case 1974: grossConverted = (long) (gross*12.1); break;
			case 1975: grossConverted = (long) (gross*10.3); break;
			case 1976: grossConverted = (long) (gross*8.8); break;
			case 1977: grossConverted = (long) (gross*7.5); break;
			case 1978: grossConverted = (long) (gross*6.7); break;
			case 1979: grossConverted = (long) (gross*5.7); break;
			case 1980: grossConverted = (long) (gross*4.7); break;
			case 1981: grossConverted = (long) (gross*4.0); break;
			case 1982: grossConverted = (long) (gross*3.4); break;
			case 1983: grossConverted = (long) (gross*2.9); break;
			case 1984: grossConverted = (long) (gross*2.7); break;
			case 1985: grossConverted = (long) (gross*2.5); break;
			case 1986: grossConverted = (long) (gross*2.3); break;
			case 1987: grossConverted = (long) (gross*2.2); break;
			case 1988: grossConverted = (long) (gross*2.1); break;
			case 1989: grossConverted = (long) (gross*2.0); break;
			case 1990: grossConverted = (long) (gross*1.9); break;
			case 1991: grossConverted = (long) (gross*1.8); break;
			case 1992: grossConverted = (long) (gross*1.7); break;
			case 1993: grossConverted = (long) (gross*1.6); break;
			case 1994: grossConverted = (long) (gross*1.5); break;
			case 1995: grossConverted = (long) (gross*1.4); break;
			case 1996: grossConverted = (long) (gross*1.4); break;
			case 1997: grossConverted = (long) (gross*1.4); break;
			case 1998: grossConverted = (long) (gross*1.3); break;
			case 1999: grossConverted = (long) (gross*1.3); break;
			case 2000: grossConverted = (long) (gross*1.3); break;
			case 2001: grossConverted = (long) (gross*1.2); break;
			case 2002: grossConverted = (long) (gross*1.2); break;
			case 2003: grossConverted = (long) (gross*1.2); break;
			case 2004: grossConverted = (long) (gross*1.2); break;
			case 2005: grossConverted = (long) (gross*1.1); break;
			case 2006: grossConverted = (long) (gross*1.1); break;
			case 2007: grossConverted = (long) (gross*1.1); break;
			case 2008: grossConverted = (long) (gross*1.1); break;
			case 2009: grossConverted = (long) (gross*1.1); break;
			case 2010: grossConverted = (long) (gross*1.1); break;
			}
			grossConverted = (long) (grossConverted / 10 + (gross * 0.9));
			return grossConverted;
		}

		public long adaptOnUsInflation(Long gross, int year)
		{
			long grossConverted = 0;
			switch(year)
			{
			case 2011: grossConverted = (long) (gross*1.1); break;
			case 2010: grossConverted = (long) (gross*1.1); break;
			case 2009: grossConverted = (long) (gross*1.1); break;
			case 2008: grossConverted = (long) (gross*1.1); break;
			case 2007: grossConverted = (long) (gross*1.2); break;
			case 2006: grossConverted = (long) (gross*1.2); break;
			case 2005: grossConverted = (long) (gross*1.2); break;
			case 2004: grossConverted = (long) (gross*1.3); break;
			case 2003: grossConverted = (long) (gross*1.3); break;
			case 2002: grossConverted = (long) (gross*1.3); break;
			case 2001: grossConverted = (long) (gross*1.3); break;
			case 2000: grossConverted = (long) (gross*1.4); break;
			case 1999: grossConverted = (long) (gross*1.4); break;
			case 1998: grossConverted = (long) (gross*1.4); break;
			case 1997: grossConverted = (long) (gross*1.5); break;
			case 1996: grossConverted = (long) (gross*1.5); break;
			case 1995: grossConverted = (long) (gross*1.6); break;
			case 1994: grossConverted = (long) (gross*1.6); break;
			case 1993: grossConverted = (long) (gross*1.6); break;
			case 1992: grossConverted = (long) (gross*1.7); break;
			case 1991: grossConverted = (long) (gross*1.7); break;
			case 1990: grossConverted = (long) (gross*1.8); break;
			case 1989: grossConverted = (long) (gross*1.9); break;
			case 1988: grossConverted = (long) (gross*2.0); break;
			case 1987: grossConverted = (long) (gross*2.1); break;
			case 1986: grossConverted = (long) (gross*2.1); break;
			case 1985: grossConverted = (long) (gross*2.2); break;
			case 1984: grossConverted = (long) (gross*2.3); break;
			case 1983: grossConverted = (long) (gross*2.4); break;
			case 1982: grossConverted = (long) (gross*2.5); break;
			case 1981: grossConverted = (long) (gross*2.7); break;
			case 1980: grossConverted = (long) (gross*3.0); break;
			case 1979: grossConverted = (long) (gross*3.4); break;
			case 1978: grossConverted = (long) (gross*3.7); break;
			case 1977: grossConverted = (long) (gross*4.0); break;
			case 1976: grossConverted = (long) (gross*4.2); break;
			case 1975: grossConverted = (long) (gross*4.5); break;
			case 1974: grossConverted = (long) (gross*5.0); break;
			case 1973: grossConverted = (long) (gross*5.5); break;
			case 1972: grossConverted = (long) (gross*5.7); break;
			case 1971: grossConverted = (long) (gross*5.9); break;
			case 1970: grossConverted = (long) (gross*6.2); break;
			case 1969: grossConverted = (long) (gross*6.6); break;
			case 1968: grossConverted = (long) (gross*6.9); break;
			case 1967: grossConverted = (long) (gross*7.1); break;
			case 1966: grossConverted = (long) (gross*7.4); break;
			case 1965: grossConverted = (long) (gross*7.5); break;
			case 1964: grossConverted = (long) (gross*7.6); break;
			case 1963: grossConverted = (long) (gross*7.7); break;
			case 1962: grossConverted = (long) (gross*7.8); break;
			case 1961: grossConverted = (long) (gross*7.8); break;
			case 1960: grossConverted = (long) (gross*8.0); break;
			case 1959: grossConverted = (long) (gross*8.1); break;
			case 1958: grossConverted = (long) (gross*8.2); break;
			case 1957: grossConverted = (long) (gross*8.5); break;
			case 1956: grossConverted = (long) (gross*8.7); break;
			case 1955: grossConverted = (long) (gross*8.8); break;
			case 1954: grossConverted = (long) (gross*8.7); break;
			case 1953: grossConverted = (long) (gross*8.8); break;
			case 1952: grossConverted = (long) (gross*8.8); break;
			case 1951: grossConverted = (long) (gross*9.2); break;
			case 1950: grossConverted = (long) (gross*10.0); break;
			case 1949: grossConverted = (long) (gross*9.7); break;
			case 1948: grossConverted = (long) (gross*9.9); break;
			case 1947: grossConverted = (long) (gross*10.9); break;
			case 1946: grossConverted = (long) (gross*12.9); break;
			case 1945: grossConverted = (long) (gross*13.1); break;
			case 1944: grossConverted = (long) (gross*13.4); break;
			case 1943: grossConverted = (long) (gross*13.8); break;
			case 1942: grossConverted = (long) (gross*14.9); break;
			case 1941: grossConverted = (long) (gross*16.6); break;
			case 1940: grossConverted = (long) (gross*16.8); break;
			case 1939: grossConverted = (long) (gross*16.7); break;
			case 1938: grossConverted = (long) (gross*16.5); break;
			case 1937: grossConverted = (long) (gross*16.6); break;
			case 1936: grossConverted = (long) (gross*17.0); break;
			case 1935: grossConverted = (long) (gross*17.2); break;
			case 1934: grossConverted = (long) (gross*17.7); break;
			case 1933: grossConverted = (long) (gross*18.1); break;
			case 1932: grossConverted = (long) (gross*16.4); break;
			case 1931: grossConverted = (long) (gross*14.7); break;
			case 1930: grossConverted = (long) (gross*13.7); break;
			case 1929: grossConverted = (long) (gross*13.7); break;
			case 1928: grossConverted = (long) (gross*13.5); break;
			case 1927: grossConverted = (long) (gross*13.4); break;
			case 1926: grossConverted = (long) (gross*13.1); break;
			case 1925: grossConverted = (long) (gross*13.5); break;
			case 1924: grossConverted = (long) (gross*13.5); break;
			case 1923: grossConverted = (long) (gross*13.9); break;
			case 1922: grossConverted = (long) (gross*13.8); break;
			case 1921: grossConverted = (long) (gross*12.3); break;
			case 1920: grossConverted = (long) (gross*12.1); break;
			}
			if(2014-year > yearAttenuation) grossConverted = (long) (grossConverted / ((double)(2014-year)/yearAttenuation));
			return grossConverted;
		}

		public long adaptOnUkInflation(Long gross, int year)
		{
			return gross;
		}

	}
	
	public class ActorInfo{
		
		public int id;
		public String name;
		public String surname;
		public int movieCounter = 1;
		
		public void setName(String str){
			String[] arr = str.split(",");
			
			surname = arr[0].trim();
			
			if (arr.length>1)
				name = arr[1].trim();
		}
		
	}
	
}
