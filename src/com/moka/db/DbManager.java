package com.moka.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.moka.interfaces.DataInterface.Element;
import com.moka.interfaces.DataInterface.Question;
import com.moka.utils.Mlog;

// TODO: Handle SQL-Injection problems.
// TODO: Modifications to the database should be in transactions when appropriate.
public class DbManager
{
	// The JDBC Connection object used to access and manipulate the database.
	private Connection co;

	private int numElements, numQuestions;

	private PreparedStatement psGetByRow;
	private static int ANSWERS_OFFSET = 1;

	public DbManager(String dbUrl, String dbName, String username, String password)
	{
		openDbConnection(dbUrl, dbName, username, password);
		createPrepStatements();

		numElements = getTableCount("element");
		numQuestions = getTableCount("question");
	}

	public void close()
	{
		closePrepStatements();
		closeDbConnection();
	}

	public int getNumElements()
	{
		return numElements;
	}

	public int getNumQuestions()
	{
		return numQuestions;
	}

	public List<Element> getElements()
	{
		List<Element> result = new ArrayList<Element>(numElements);

		try
		{
			Statement stmt = co.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT id, name, n_guesses FROM element;");

			Element element;

			while (rs.next())
			{
				element = new Element();
				element.idDatabase = rs.getInt("id");
				element.name = rs.getString("name");
				element.numGuesses = rs.getInt("n_guesses");
				result.add(element);
			}

			rs.close();
			stmt.close();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
			System.exit(1);
		}

		return result;
	}

	public List<Question> getQuestions()
	{
		List<Question> result = new ArrayList<Question>(numQuestions);

		try
		{
			Statement stmt = co.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT id, text, global_effectiveness FROM question;");

			Question question;

			while (rs.next())
			{
				question = new Question();
				question.idDatabase = rs.getInt("id");
				question.text = rs.getString("text");
				question.globalEffectiveness = rs.getFloat("global_effectiveness");
				result.add(question);
			}

			rs.close();
			stmt.close();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
			System.exit(1);
		}

		return result;
	}

	/** Returns all the answer values for a given Element (identified by the database ID). */
	public float[] getAnswersForElement(int databaseElementId)
	{
		float [] result = new float[numQuestions];

		try
		{
			psGetByRow.setInt(1, databaseElementId);
			ResultSet rs = psGetByRow.executeQuery();

			// Checks that the  number of questions is really equal to the num of columns
			if(rs.getMetaData().getColumnCount() != numQuestions+ANSWERS_OFFSET) return null;

			for(int i=0; i<numQuestions; i++) result[i] = rs.getFloat(i+ANSWERS_OFFSET+1);

			rs.close();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
			System.exit(1);
		}

		return result;
	}

	/** Returns a matrix of all the answer values. */
	public float[][] getAnswers()
	{
		float[][] result = new float[numElements][numQuestions];

		if(numElements != getTableCount("answer")) return null;

		try
		{
			Statement stmt = co.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM answer;");

			// Checks that the  number of questions is really equal to the num of columns
			if(rs.getMetaData().getColumnCount() != numQuestions+ANSWERS_OFFSET) return null;

			for(int i=0; i<numElements && rs.next(); i++)
			{
				for(int j=0; j<numQuestions; j++) result[i][j] = rs.getFloat(j+ANSWERS_OFFSET+1);
			}

			rs.close();
			stmt.close();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
			System.exit(1);
		}

		return result;
	}

	public int addElement(String name)
	{
		int newElementId = -1;

		try
		{
			Statement stmt = co.createStatement();

			String sqlInsert = "INSERT INTO element(name, n_guesses) VALUES ('"+name+"', 0);";
			newElementId = stmt.executeUpdate(sqlInsert, Statement.RETURN_GENERATED_KEYS);

			ResultSet rs = stmt.getGeneratedKeys();
			if (rs.next()) newElementId = rs.getInt(1);

			stmt.executeUpdate("INSERT INTO answer(element_id) VALUES ("+newElementId+");");

			rs.close();
			stmt.close();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
			System.exit(1);
		}

		return newElementId;
	}

	public void removeElement(int databaseElementId)
	{
		try
		{
			Statement stmt = co.createStatement();

			stmt.executeUpdate("DELETE FROM element WHERE id = "+databaseElementId+";");
			stmt.executeUpdate("DELETE FROM answer WHERE element_id = "+databaseElementId+";");

			stmt.close();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
			System.exit(1);
		}
	}

	public int addQuestion(String text)
	{
		int newQuestionId = -1;

		try
		{
			Statement stmt = co.createStatement();

			String sqlInsert = "INSERT INTO question(text, global_effectiveness) VALUES ('"+text+"', 0);";
			stmt.executeUpdate(sqlInsert, Statement.RETURN_GENERATED_KEYS);

			ResultSet rs = stmt.getGeneratedKeys();
			if (rs.next()) newQuestionId = rs.getInt(1);

			stmt.executeUpdate("ALTER TABLE answer ADD Q"+newQuestionId+" FLOAT NOT NULL DEFAULT '0';");

			rs.close();
			stmt.close();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
			System.exit(1);
		}

		return newQuestionId;
	}

	public void removeQuestion(int databaseQuestionId)
	{
		try
		{
			Statement stmt = co.createStatement();

			stmt.executeUpdate("DELETE FROM question WHERE id = "+databaseQuestionId+";");
			stmt.executeUpdate("ALTER TABLE answer DROP COLUMN Q"+databaseQuestionId+";");

			stmt.close();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
			System.exit(1);
		}
	}

	public void setAnswer(int databaseElementId, int databaseQuestionId, float answer)
	{
		try
		{
			Statement stmt = co.createStatement();
			stmt.executeUpdate("UPDATE answer SET Q"+databaseQuestionId+" = "+answer+" WHERE element_id = "+databaseElementId+";");

			stmt.close();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
			System.exit(1);
		}	
	}

	public void setAnswers(int databaseQuestionId, HashMap<Integer,Float> answers)
	{
		
		PreparedStatement psUpdate = null;
		
		try
		{
			co.setAutoCommit(false);
			psUpdate = co.prepareStatement("UPDATE answer SET Q"+databaseQuestionId+" = ? WHERE element_id = ? ;");
			int batchCounter = 0;
			
			for (Map.Entry<Integer,Float> entry : answers.entrySet()) {
				
				psUpdate.setInt(1,entry.getKey());
				psUpdate.setFloat(2,entry.getValue());
				
				//add the new sql to the batch
				psUpdate.addBatch();
				
				// flush the batch every 1000 items
				if ((batchCounter + 1) % 1000 == 0)
					psUpdate.executeBatch();
					
				batchCounter++; //increase the batch counter
			
			}

			// Execute the last batch
			psUpdate.executeBatch();
			co.commit();
			
			//restore autocommit
			co.setAutoCommit(true);

			
		}
		catch (SQLException e)
		{
			e.printStackTrace();
			System.exit(1);
		}
		finally { //close the prepared statement
			if (psUpdate != null) try { psUpdate.close(); } catch (SQLException e1) {}
		}

	}

	// --- private methods ---	

	private void openDbConnection(String dbUrl, String dbName, String username, String password)
	{
		try
		{
			Class.forName("com.mysql.jdbc.Driver");
			co = DriverManager.getConnection("jdbc:mysql://"+dbUrl+"/"+dbName+"?useServerPrepStmts=false&rewriteBatchedStatements=true&user="+username+"&password="+password+"");
		}
		catch (ClassNotFoundException e)
		{
			Mlog.e("DbManager", e.getMessage());
			System.exit(1);
		}
		catch (SQLException e)
		{
			Mlog.e("DbManager", e.getMessage());
			System.exit(1);
		}
	}

	private void closeDbConnection()
	{
		try
		{
			co.close();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
			System.exit(1);
		}
	}

	/** Returns the number of rows of the given table. */
	private int getTableCount(String tableName)
	{
		int tableCount = 0;

		try
		{
			Statement stmt = co.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT COUNT(*) FROM "+tableName+";");
			if (rs.next()) tableCount = rs.getInt(1);

			rs.close();
			stmt.close();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
			System.exit(1);
		}

		return tableCount;
	}

	private void createPrepStatements()
	{
		try
		{
			psGetByRow = co.prepareStatement("SELECT * FROM answer WHERE element_id = ?;");
		}
		catch (SQLException e)
		{
			e.printStackTrace();
			System.exit(1);
		}
	}

	private void closePrepStatements()
	{
		try
		{
			if(psGetByRow != null) psGetByRow.close();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
			System.exit(1);
		}
	}

}




