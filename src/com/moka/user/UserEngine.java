package com.moka.user;

import com.moka.interfaces.DataInterface;
import com.moka.interfaces.GameInterface;
import com.moka.interfaces.GameInterface.gameMode;

public class UserEngine {
	
	private GameInterface gameEngine;
	private ConsoleUser consoleUser;
	private DataInterface dataManager;

	public UserEngine(GameInterface ge, DataInterface dm) {
		
		gameEngine = ge;
		dataManager = dm;
		consoleUser = new ConsoleUser();
		
		String question = "", guess = "";
		int questionN; float answer;
		
		
		do{
			
			consoleUser.showMovies(dataManager.getElements());
			
			questionN = 0;
			gameEngine.startGame(gameMode.NORMAL);
			
			while( (question = gameEngine.getQuestion()) != null){
				
				questionN++;
				consoleUser.printQuestion(questionN,question);
				
				answer = consoleUser.readAnswer();
				gameEngine.sendAnswerToQuestion(answer);
			
			}
			
			guess = gameEngine.endGame();
			consoleUser.guessMovie(guess);
			
			consoleUser.showGameReport(gameEngine.getReport());
			
		}while( consoleUser.playAgain() );
	}

}
