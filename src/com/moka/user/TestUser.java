package com.moka.user;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import com.moka.interfaces.DataInterface;

public class TestUser {


	private TestReport testReport;
	private DataInterface dataManager;
	private HashMap<String,Integer> questions;
	
	private int mode = 0;
	private int stopGuessNumber = 0;
	private int currentPick = 0;
	private int questionN = 0;
	private int errorPosition = 1;
	private int secondErrorPosition = 2;
	private int errorsN = 0;

	public TestUser(DataInterface dm, int m, int sgn){
		dataManager = dm;
		mode = m;
		stopGuessNumber = sgn;
		
		testReport = new TestReport();
		questions = new HashMap<String, Integer>(dataManager.getNumQuestions());
		
		for(int q=0; q < dataManager.getNumQuestions(); q++)
			questions.put(dataManager.getQuestion(q).text, q);
		
		testReport.elapsedTime = System.nanoTime();
		
		if(mode > 1) stopGuessNumber = Integer.MAX_VALUE;
	}

	public void pickMovie()
	{
		if(mode < 1) currentPick = new Random().nextInt(dataManager.getNumElements());
		questionN = 0;
		errorsN = 0;
	}

	public float answerQuestion(String question)
	{
		testReport.totalQuestions++;
		questionN++;

		int questionIndex = questions.get(question);
		float answerForQuestion = dataManager.getAnswer(currentPick, questionIndex);
		
		return this.getAnswer(answerForQuestion);
	}	
	
	public float getAnswer(float answerForQuestion)
	{
		switch(mode){
		
			case 0: return Math.round(answerForQuestion);
			
			case 1: 
				int rand = new Random().nextInt(100)*(questionN+90);
				if(rand > 9000)
				{
					testReport.wrongAnswers++;
					errorsN++;
					if(answerForQuestion > 0) return -1;
					else return 1;
				}
				return Math.round(answerForQuestion);
				
			case 2:
				if(questionN == errorPosition)
				{
					testReport.wrongAnswers++;
					errorsN++;
					if(answerForQuestion > 0) return -1;
					else return 1;
				}
				else return Math.round(answerForQuestion);
				
			case 3:
				if(questionN == errorPosition || questionN == secondErrorPosition)
				{
					testReport.wrongAnswers++;
					errorsN++;
					if(answerForQuestion > 0) return -1;
					else return 1;
				}
				else return Math.round(answerForQuestion);
				
			default: return 0;
		}
		
	}
	
	public void checkGuess(String guess) 
	{
		String pickedMovie = dataManager.getElement(currentPick).name;
		
		testReport.testsExecuted++;
		
		if(questionN > testReport.maxQAsked)
		{ 
			testReport.maxQAsked = questionN;
			testReport.maxQMovie.clear();
			testReport.maxQMovie.add(pickedMovie);
		}
		if(questionN == testReport.maxQAsked && !testReport.maxQMovie.contains(pickedMovie)) testReport.maxQMovie.add(pickedMovie);

		
		
		if(questionN < testReport.minQAsked)
		{ 
			testReport.minQAsked = questionN;
			testReport.minQMovie.clear();
			testReport.minQMovie.add(pickedMovie);
		}
		if(questionN == testReport.minQAsked && !testReport.minQMovie.contains(pickedMovie)) testReport.minQMovie.add(pickedMovie);
		
		
		
		if(guess.equals(pickedMovie)) testReport.testsSuccesful++;
		else
		{
			testReport.testsFailed++;
			ErrorReport er = new ErrorReport();
			er.pick = pickedMovie;
			er.guess = guess;
			er.errorPosition = errorPosition;
			er.questionsAsked = questionN;
			er.totalErrors = errorsN;
			testReport.failedMovies.add(er);
		}


		
		if(mode == 2)
		{
			if(questionN < errorPosition)
			{
				currentPick++;
				errorPosition = 1;
				if(currentPick >= dataManager.getNumElements()) stopGuessNumber = 0;
			}
			else
			{
				errorPosition++;
			}
		}
		
		if(mode == 3)
		{
			if(questionN < errorPosition)
			{
				currentPick++;
				errorPosition = 1;
				secondErrorPosition = 2;
				if(currentPick >= dataManager.getNumElements()) stopGuessNumber = 0;
			}
			else
			{
				if(questionN < secondErrorPosition)
				{
					errorPosition++;
					secondErrorPosition = errorPosition + 1;
				}
				else
				{
					secondErrorPosition++;
				}
			
		}
		}
	}
	
	// if tests completed < total tests scheduled continue with tests
	public boolean playAgain()
	{
		if(testReport.testsExecuted < stopGuessNumber) return true;
		else return false;
	}

	public void endTest() {
		testReport.elapsedTime = System.nanoTime()-testReport.elapsedTime;
		printTestReport();
	}
	
	public void printTestReport() 
	{

		System.out.println("\n------------------------------------------------------------------------------------------------------------------------------------------------------\n");
		System.out.println("  Report of the test: \n");

		System.out.println(String.format("%30s","Tests executed: ") + testReport.testsExecuted);
		System.out.println(String.format("%30s","Successful guesses: ") + testReport.testsSuccesful);
		System.out.println(String.format("%30s","Failed guesses: ") + testReport.testsFailed);
		System.out.println(String.format("%30s","Failed percent: ") + String.format("%.3g", (float) testReport.testsFailed/testReport.testsExecuted*100) + "%");
		System.out.println(String.format(""));
		System.out.println(String.format("%30s","Total questions asked: ") + testReport.totalQuestions);
		System.out.println(String.format("%30s","Total wrong answers returned: ") + testReport.wrongAnswers);
		System.out.println(String.format("%30s","Max questions for guess: ") + testReport.maxQAsked);
		System.out.println(String.format("%30s","for movies: ") + testReport.maxQMovie);
		System.out.println(String.format("%30s","Min questions for guess: ") + testReport.minQAsked);
		System.out.println(String.format("%30s","for movies: ") + testReport.minQMovie);
		System.out.println(String.format("%30s","Medium questions for guess: ") + String.format("%.3g", (float) testReport.totalQuestions/testReport.testsExecuted));
		System.out.println(String.format(""));
		System.out.println(String.format("%30s","Time required: ") + TimeUnit.MILLISECONDS.convert(testReport.elapsedTime, TimeUnit.NANOSECONDS) + " ms.");

		if( mode < 3 )
		{
			System.out.println("\n------------------------------------------------------------------------------------------------------------------------------------------------------\n");
			System.out.println("  List of failed guesses: \n");
			
			System.out.println(	String.format("%30s","Picked movie: ") + String.format("%30s","Guessed movie: ") + String.format("%15s","Error at: ") + String.format("%15s","Tot quest: ") + String.format("%15s","Tot err: ") );
			
			for(ErrorReport er : testReport.failedMovies)
			{
				System.out.println(	String.format("%30s",er.pick) + 
									String.format("%30s",er.guess) + 
									String.format("%15s",er.errorPosition) + 
									String.format("%15s",er.questionsAsked) + 
									String.format("%15s",er.totalErrors) );
			}
		}

		System.out.println("\n------------------------------------------------------------------------------------------------------------------------------------------------------\n");
		
	}

	
	
	
//---- REPORT CLASSES ------------------------------------------------
	
	public class TestReport
	{
		public long elapsedTime = 0;
		public int testsExecuted = 0;
		public int testsSuccesful = 0;
		public int testsFailed = 0;
		public int totalQuestions = 0;
		public int wrongAnswers = 0;
		
		public int maxQAsked = 0;
		public List<String> maxQMovie = new ArrayList<String>();
		
		public int minQAsked = 100;
		public List<String> minQMovie = new ArrayList<String>();
		
		public List<ErrorReport> failedMovies = new ArrayList<ErrorReport>();
	}
	
	public class ErrorReport
	{
		public String pick;
		public String guess;
		public int errorPosition;
		public int questionsAsked;
		public int totalErrors;
	}


}
	