package com.moka.user;

import com.moka.interfaces.DataInterface;
import com.moka.interfaces.GameInterface;
import com.moka.interfaces.GameInterface.gameMode;

public class TestEngine {
	/*
	 * RANDOM MODES: 0 = no errors, 1 = increasing errors on questions number
	 * DETERMINISTIC MODES: 2 = one error per guess, 3 = two errors per guess
	 */
	private final int mode = 2; //0 = no-error, 1 = best user simulation 2 = 1 error per guess, 3 = 2 errors per guess
	private final int stopGuessNumber = 1000;
	
	private GameInterface gameEngine;
	private TestUser testUser;
	private DataInterface dataManager;

	public TestEngine(GameInterface ge, DataInterface dm) {
		
		gameEngine = ge;
		dataManager = dm;
		testUser = new TestUser(dataManager,mode,stopGuessNumber);
		
		String question = "", guess = "";
		float answer;
		
		do{
			gameEngine.startGame(gameMode.NORMAL);
			testUser.pickMovie();
			
			question = gameEngine.getQuestion();
			
			while(question != null){
				
				answer = testUser.answerQuestion(question);
				gameEngine.sendAnswerToQuestion(answer);

				question = gameEngine.getQuestion();
			}
			
			guess = gameEngine.endGame();
			testUser.checkGuess(guess);
			
		}while( testUser.playAgain() );
		
		testUser.endTest();
	}

}
