package com.moka.user;

import com.moka.db.DbManager;
import com.moka.interfaces.DataInterface;

public class UpdateDatabase {

	private ConsoleUser consoleUser;
	private DataInterface dataManager;
	private DbManager dbManager;

	public UpdateDatabase(DataInterface dm, DbManager dbm) {

		dataManager = dm;
		dbManager = dbm;
		consoleUser = new ConsoleUser();

		boolean continueUpdate = true;
		while(continueUpdate){

			switch( consoleUser.newUpdate() ){

			//add a question
			case 1: 
				
				String text = consoleUser.getQuestionText();
				
				float[] questionAnswers = new float[dataManager.getNumElements()];
				
				for(int e = 0; e<dataManager.getNumElements(); e++)
					questionAnswers[e] = consoleUser.getAnswer(dataManager.getElement(e));
				
				if(!consoleUser.confirmAnswers()) break;
				
				//CAUTION: the following code writes directly on the DATABASE  ---------------------------------------
				int newQid = dbManager.addQuestion(text);
				
				for(int e = 0; e<dataManager.getNumElements(); e++)
					dbManager.setAnswer(dataManager.getElement(e).idDatabase, newQid, questionAnswers[e]);
				//----------------------------------------------------------------------------------------------------
				
				break;
			
			//add an element
			case 2:

				String name = consoleUser.getMovieName();
				
				float[] elementAnswers = new float[dataManager.getNumQuestions()];
				
				for(int q = 0; q<dataManager.getNumQuestions(); q++)
					elementAnswers[q] = consoleUser.getAnswer(dataManager.getQuestion(q));

				if(!consoleUser.confirmAnswers()) break;
				
				//CAUTION: the following code writes directly on the DATABASE  ---------------------------------------
				int newEid = dbManager.addElement(name);
				
				for(int e = 0; e<dataManager.getNumQuestions(); e++)
					dbManager.setAnswer(newEid, dataManager.getQuestion(e).idDatabase, elementAnswers[e]);
				//----------------------------------------------------------------------------------------------------
				
				break;

			default: continueUpdate = false;
			break;

			}
			

		}

	}

}
