package com.moka.user;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import com.moka.interfaces.DataInterface;
import com.moka.interfaces.DataInterface.Question;
import com.moka.interfaces.GameInterface;
import com.moka.interfaces.GameInterface.gameMode;
import com.moka.utils.Mlog;

public class TestEngineFra {

	private GameInterface gameEngine;
	private ConsoleUser consoleUser;
	private DataInterface dataStructure;

	public TestEngineFra(GameInterface ge, DataInterface ds) {

		gameEngine = ge;
		dataStructure = ds;
		consoleUser = new ConsoleUser();

		String question = "", guess = "";
		int questionN; float answer;


		int max_errors = 1;
		int runs_num = ds.getNumQuestions();

		HashMap<String, Integer> questionsMap = new HashMap<String, Integer>();

		ArrayList<TestReport> reportList = new ArrayList<TestReport>();

		//get the questions
		List<Question> qList = ds.getQuestions();

		for(int i = 0; i<qList.size(); i++ )
			questionsMap.put(qList.get(i).text, i);


		
		long start = System.currentTimeMillis();
		
		//simula di pensare un film alla volta
		for(int i=0; i<ds.getNumElements(); i++){

			//Mlog.w("Testing script", "Penso: "+ds.getElement(i).name);

			int incrementalInt = 0;

			//per ogni film, faccio runs_num prove
			for(int j=0; j<runs_num ; j++){

				TestReport report = new TestReport();

				ArrayList<Integer> error_position = new ArrayList<Integer>();

				//se ho 1 errore e tante run quante sono le domande, non genero casuale, ma provo tutti
				if(max_errors == 1 && runs_num == ds.getNumQuestions() ){
					error_position.add(incrementalInt++);
				}else{
					//randomizzo la posizione degli errori
					for(error_position.size(); error_position.size()<max_errors;){
						Random r = new Random();
						int randomInt = r.nextInt(ds.getNumQuestions());
						if( error_position.contains(randomInt) )
							continue;
						//elseadd it to the array
						error_position.add(randomInt);

					}
				}

				questionN = 0;
				//ora che ho generato la posizione degli errori, inizio a rispondere
				gameEngine.startGame(gameMode.NORMAL);
				while( (question = gameEngine.getQuestion()) != null){

					report.questions_asked.add(question);
					float expected_answer = ds.getAnswer(i, questionsMap.get(question));

					//controllo se devo sbagliare questa risposta
					if( error_position.contains(questionN) ){
						answer = expected_answer<0 ? 1 : -1; 
					}else{ // rispondo giusto
						answer = Math.round(expected_answer);
					}

					gameEngine.sendAnswerToQuestion(answer);
					questionN++;


				}
				//quando ricevo null esco e controllo se ho indovinato
				guess = gameEngine.endGame();

				report.elementThought = ds.getElement(i).name;
				report.elementGuessed = guess;
				report.n_questions = questionN;
				report.error_positions = error_position;

				if(!guess.equals(ds.getElement(i).name)){ //ho sbagliato
					report.error = true;
				}

				reportList.add(report);

			}


			//quando ha finito, pensa al prossimo film,
		}
		
		long simulation_elapsed_time = System.currentTimeMillis() - start;
		System.out.println();


		//print the reports

		int e_str_len = 40;
		int n_str_len = 15;
		char[] delimiter;
		
		//print the stats
		System.out.println(String.format("%"+e_str_len+"s%"+e_str_len+"s","Simulations Stats",""));
		delimiter = new char[e_str_len+e_str_len];
		Arrays.fill(delimiter, '-');
		System.out.println(new String(delimiter));
		
		System.out.println(String.format("%"+e_str_len+"s%"+e_str_len+"d","Elapsed Time (msecs)",simulation_elapsed_time));
		System.out.println(String.format("%"+e_str_len+"s%"+e_str_len+"d","#Simulations",reportList.size()));
		//conto alcune statistiche interne
		int succ_guesses = 0,fail_guesses = 0, tot_q_asked = 0;
		for(TestReport report : reportList){
			if(report.error) fail_guesses++;
			else succ_guesses++;
			
			tot_q_asked +=report.n_questions;
			
		}
		System.out.println(String.format("%"+e_str_len+"s%"+e_str_len+"d","Successful guesses",succ_guesses));
		System.out.println(String.format("%"+e_str_len+"s%"+e_str_len+"d","Failed guesses",fail_guesses));
		System.out.println(String.format("%"+e_str_len+"s%"+e_str_len+"d","Total #Questions",tot_q_asked));
		System.out.println(String.format("%"+e_str_len+"s%"+e_str_len+"d","Avrg #Questions per Element",tot_q_asked / reportList.size()));


		//Print the average values
		System.out.println(String.format("\n\n%"+e_str_len+"s%"+n_str_len+"s%"+n_str_len+"s%"+n_str_len+"s%"+n_str_len+"s","Elem. Thought", "#Failures", "Avrg #Quest.","Min #Quest","Max #Quest"));
		delimiter = new char[e_str_len+n_str_len+n_str_len+n_str_len+n_str_len];
		Arrays.fill(delimiter, '-');
		System.out.println(new String(delimiter));
		String last_elem="";
		int min_q = ds.getNumQuestions();
		int max_q = 0;
		float elem_tot_games=0.0f;
		int total_q = 0;
		int fail_g = 0;
		for(TestReport report : reportList){

			if(report.error){ fail_g++;  continue; }

			if(!report.elementThought.equals(last_elem)){ //se mi sto posizionando su uno nuovo azzero
				
				if(elem_tot_games>0)
					System.out.println(String.format("%"+e_str_len+"s%"+n_str_len+"d%"+n_str_len+".2f%"+n_str_len+"s%"+n_str_len+"s",last_elem,fail_g,total_q/elem_tot_games,min_q,max_q));
				
				min_q = ds.getNumQuestions();
				max_q = 0;
				elem_tot_games = 0;
				total_q = 0;
				fail_g = 0;
				last_elem = report.elementThought;
			}

			
			//ricerca del massimo
			if(report.n_questions > max_q){
				max_q = report.n_questions;
			}

			//ricerca del minimo
			if(report.n_questions < min_q){
				min_q = report.n_questions;
			}
			
			elem_tot_games++;
			total_q += report.n_questions;



		}
		//stampo l'ultimo
		System.out.println(String.format("%"+e_str_len+"s%"+n_str_len+"d%"+n_str_len+".2f%"+n_str_len+"s%"+n_str_len+"s",last_elem,fail_g,total_q/elem_tot_games,min_q,max_q));



		System.out.println(String.format("\n\n%"+e_str_len+"s%"+e_str_len+"s%"+n_str_len+"s%"+n_str_len+"s","Elem. Thought","Elem. Guessed","# Quest.","Wrong Answ."));
		delimiter = new char[e_str_len+e_str_len+n_str_len+n_str_len];
		Arrays.fill(delimiter, '-');
		System.out.println(new String(delimiter));

		for(TestReport report : reportList){

			if(report.error){
				
				String q = report.questions_asked.get(report.error_positions.get(0)).substring(0, 20);
				
				System.out.println(String.format("%"+e_str_len+"s%"+e_str_len+"s%"+n_str_len+"s%"+n_str_len+"s",report.elementThought,report.elementGuessed,report.n_questions,q));
			}

		}


	}

	public class TestReport {

		public String elementThought;
		public String elementGuessed;
		public boolean error = false;
		public int n_questions;
		public ArrayList<String> questions_asked = new ArrayList<String>();
		public ArrayList<Integer> error_positions = new ArrayList<Integer>();

	}

}
