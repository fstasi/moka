package com.moka.user;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import com.moka.interfaces.DataInterface.Element;
import com.moka.interfaces.DataInterface.Question;
import com.moka.interfaces.GameInterface.ReportRow;
import com.moka.utils.Mlog;

public class ConsoleUser {

	//specify the name of the task - for debugging purpose
	private static final String className = "Input";

	public ConsoleUser(){}



	//----- INPUT FUNCTIONS ------------------------------------------------------------------------------------------------------------------------------------------------------------------

	// Read the answer from the user input, update probability and guess the winner
	public float readAnswer(){

		String str = readInput();

		if(str.equals("")) return 0;

		if(str.equals("y") || str.equals("yes")){
			return 1;
		}
		if(str.equals("n") || str.equals("no")){
			return -1;
		}

		System.out.println("'" + str + "' is not a valid answer. Options are 'y' or 'n'.");
		float answer = readAnswer();
		return answer;	
	}

	// read a string from the user input
	public String readInput(){	

		String str = null;
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

		try { str = reader.readLine(); }
		catch (IOException e) 
		{
			Mlog.e(className, e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
		return str;
	}



	//----- OUTPUT FUNCTIONS ------------------------------------------------------------------------------------------------------------------------------------------------------------------

	//present the movies to the user
	public void showMovies(List<Element> list){

		System.out.println("------------------------------------------------------------------------------------------------------------------------------------------------------");
		System.out.println("Please, choose a movie in the following list:\n");

		for( int m=0; m<list.size(); m++ ){
			System.out.println(String.format("%3s", m+1) + ": " + list.get(m).name);
		}

		System.out.println("\nPress ENTER when you decided.");
		System.out.println("------------------------------------------------------------------------------------------------------------------------------------------------------");

		readInput();
	}


	public void printQuestion(int questionN, String question) {
		System.out.println("Question number " + questionN + ": " + question + " Press ENTER if you don't know the answer.");
	}

	public void guessMovie(String guess) {
		System.out.println("\nThe Guessed Movie is: " + guess);
	}

	public boolean playAgain() {

		System.out.println("Do you want to play again?\n");

		if(readInput().equals("y")) return true;
		else{
			System.out.println("Thanks for playing! Goodbye!");
			return false;
		}
	}



	public void showGameReport(List<ReportRow> report) {

		System.out.println("\n------------------------------------------------------------------------------------------------------------------------------------------------------\n");
		System.out.println("Report of the game:");

		for( int rr=0; rr<report.size(); rr++ ){
			
			String err = Math.abs(report.get(rr).answerExpected - report.get(rr).answerGiven)/2 > 0.5 ? "X" : "";
			
			System.out.println(String.format("%75s", report.get(rr).questionName) + 
					""+ String.format("%5s",err) + 
					"     Answer given: " + String.format("%4s",report.get(rr).answerGiven) + 
					"     Answer expected: " + String.format("%4s",report.get(rr).answerExpected));
		}

		System.out.println("\n------------------------------------------------------------------------------------------------------------------------------------------------------\n");

	}



	public int newUpdate() {

		System.out.println("Do you want to update the database? ('k' to insert a Question / 'e' to insert an element)");

		String str = readInput();

		if(str.toLowerCase().equals("k")){
			return 1;
		}
		if(str.toLowerCase().equals("e")){
			return 2;
		}

		return 0;
	}



	public String getQuestionText() {
		System.out.print("Please insert the question text: ");
		return readInput();
	}

	public String getMovieName() {
		System.out.print("Please insert the Movie title: ");
		return readInput();
	}

	
	
	private float stringToAnswer(String str) throws NumberFormatException {
		
		float ans;
		
		if(str.toLowerCase().equals("y") || str.toLowerCase().equals("yes")){
			ans = 1;
		}
		else if(str.toLowerCase().equals("n") || str.toLowerCase().equals("no")){
			ans = -1;
		}
		else{
			ans = Float.parseFloat(str);
		}
		
		return ans;
		
	}
	

	public float getAnswer(Element element) {
		System.out.print("Answer for " + element.name + ": ");

		float ans;
		try{
			ans = stringToAnswer(readInput()) ;
		}catch (NumberFormatException e){
			System.out.println("Please insert only float values.");
			ans = getAnswer(element);
		}

		return ans;
	}



	public float getAnswer(Question question) {
		System.out.print(question.text + " ");

		float ans;
		try{
			ans = stringToAnswer(readInput()) ;
		}catch (NumberFormatException e){
			System.out.println("Please insert only float values.");
			ans = getAnswer(question);
		}

		return ans;
	}



	public boolean confirmAnswers() {
		
		System.out.println("\n --- Do you confirm all the answers? (CAUTION: everything will be permanently written on the DB) ---");

		String str = readInput();
		
		if(str.equals("y") || str.equals("Y") || str.equals("yes") || str.equals("YES")) return true;
		else{
			System.out.println("\nAborting...\n");
			return false;
		}
	}

}

