package com.moka.interfaces;

import java.util.List;

public interface GameInterface
{
	public enum gameMode
	{
		NORMAL, LEARNING, DEBUG, LEARNING_DEBUG
	}

	/** Starts the game, resetting the current session. 
	 * Returns a boolean indicating if the game has started correctly (true) or a problem happened (false). 
	 * */
	public boolean startGame(gameMode mode);
	
	/** Returns the current question to answer to, or null if there is an answer. */
	public String getQuestion();
	
	/** Sends an answer to the current question, with a value in the range -1.0 to +1.0.
	 * Returns true if the answer is in the correct range, false otherwise. */
	public boolean sendAnswerToQuestion(float answer);
	
	/** Returns the current answer. The string identifies the element. */
	public String endGame();
	
	
	public static class ReportRow
	{
		public String questionName;
		public float answerGiven;
		public float answerExpected;
	}
	
	/** Returns a list of the asked questions, answers given and expected for the current game. */
	public List<ReportRow> getReport();

}

