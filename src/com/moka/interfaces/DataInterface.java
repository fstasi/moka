package com.moka.interfaces;

import java.util.List;

public interface DataInterface
{
	public static class Element
	{
		public int idDatabase;
		public String name;
		public int numGuesses;
	}

	public static class Question
	{
		public int idDatabase;
		public String text;
		public float globalEffectiveness;
	}

	public List<Element> getElements();
	public List<Question> getQuestions();

	public Element getElement(int elementIndex);
	public Question getQuestion(int questionIndex);

	public int getNumElements();
	public int getNumQuestions();

	public void addQuestion(String questionText);
	public void addElement(String elementText);

	/** All set methods can return false if the given answer(s) are out of range. */

	public float getAnswer(int elementIndex, int questionIndex);
	public float[][] getAnswers();
	public boolean setAnswer(int elementIndex, int questionIndex, float answer);

	public float[] getAnswersForQuestion(int questionIndex);
	public boolean setAnswersForQuestion(int questionIndex, List<Float> answers);

	public float[] getAnswersForElement(int elementIndex);
	public boolean setAnswersForElement(int elementIndex, List<Float> answers);
}
