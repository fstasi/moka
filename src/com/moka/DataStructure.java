package com.moka;

import com.moka.db.DbManager;
import com.moka.interfaces.DataInterface;

import java.util.ArrayList;
import java.util.List;

public class DataStructure implements DataInterface {

	private int num_elems;
	private int num_keys;
	private int num_picks;
	
	private DbManager dbManager;
	
	static final float MIN_RANGE = -1.0f;
	static final float MAX_RANGE = 1.0f;

	private ArrayList<Element> elementList;
	private ArrayList<Question> questionList;

	//list of answers array. Each row refers to a keyword. Each array refers to the answers each Element gave for the given keyword
	private float[][] answers;

	private float[] elem_probabilities;
	
	public DataStructure(DbManager dbManager) {
		
		this.dbManager = dbManager;
		
		//read from the database and populate the internal structure
		this.num_elems = this.dbManager.getNumElements();
		this.num_keys = this.dbManager.getNumQuestions();
		
		this.elementList = (ArrayList<Element>) this.dbManager.getElements();
		this.questionList = (ArrayList<Question>) this.dbManager.getQuestions();
		
		this.answers = this.dbManager.getAnswers();	
		
	}


	public static class Statistics 
	{
		public int totalGuesses;
	}
	
	public boolean validRange(float answer){
		
		if(answer>=MIN_RANGE && answer<=MAX_RANGE)
			return true;
		
		return false;
		
	}
	
	public int getNum_picks() {
		return num_picks;
	}


	@Override
	public List<Element> getElements() {
		return this.elementList;
	}


	@Override
	public List<Question> getQuestions() {
		return this.questionList;
	}


	@Override
	public Element getElement(int idElement) {
		return this.elementList.get(idElement);
	}


	@Override
	public Question getQuestion(int idQuestion) {
		return this.questionList.get(idQuestion);
	}


	@Override
	public int getNumElements() {
		return this.num_elems;
	}


	@Override
	public int getNumQuestions() {
		return this.num_keys;
	}


	@Override
	public void addQuestion(String questionText) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void addElement(String elementText) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public float[][] getAnswers() {
		return this.answers;
	}
	
	@Override
	public float getAnswer(int idElement, int idQuestion) {
		return this.answers[idElement][idQuestion];
	}


	@Override
	public boolean setAnswer(int idElement, int idQuestion, float answer) {
		
		if(validRange(answer))
			this.answers[idElement][idQuestion] = answer;
		else
			return false;
		
		return true;
	}


	@Override
	public float[] getAnswersForQuestion(int idQuestion) {
		
		float[] answerList = new float[this.num_elems];
		
		
		for(int i=0; i<this.num_elems; i++){
			answerList[i]=this.answers[i][idQuestion];
		}
		
		return answerList;
	}


	@Override
	public boolean setAnswersForQuestion(int idQuestion, List<Float> answers) {
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	public float[] getAnswersForElement(int idElement) {
		
		return this.answers[idElement];
	}


	@Override
	public boolean setAnswersForElement(int idElement, List<Float> answers) {
		// TODO Auto-generated method stub
		return false;
	}



}
