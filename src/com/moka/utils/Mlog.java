package com.moka.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.ConsoleHandler;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

public class Mlog {
	
	private static Logger Log = Logger.getLogger("InfoLogging");
	private static boolean m_Enabled = true;
	private static Level m_Level = Level.FINER;
	private static Handler ch = null;

	static{
	    try {
	    	Log.setLevel(Level.ALL);
		
	        Formatter formatter = new Formatter() {

	            @Override
	            public String format(LogRecord arg0) {
	                StringBuilder b = new StringBuilder();
	                b.append(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
	                b.append(" - ");
	                b.append(arg0.getMessage());
	                b.append(System.getProperty("line.separator"));
	                return b.toString();
	            }

	        };

	        ch = new ConsoleHandler();
	        ch.setFormatter(formatter);
	        ch.setLevel(m_Level);
	        Log.addHandler(ch);
	        Log.setUseParentHandlers(false);

/*
 			//save logs on file
	        Handler fh = new FileHandler("log.txt");
	        fh.setFormatter(formatter);
	        Log.addHandler(fh);
*/
	        
	        LogManager lm = LogManager.getLogManager();
	        lm.addLogger(Log);
	    }
	    catch (Throwable e) {
	        e.printStackTrace();
	    }
	}

	public static void e(String tag, String msg) {
		if (m_Enabled) Log.severe(tag + " | Error: " + msg);
	}

	public static void w(String tag, String msg) {
		if (m_Enabled) Log.warning(tag + " | Warning: " + msg);
	}

	public static void i(String tag, String msg) {
		if (m_Enabled) Log.info(tag + " | Info: " + msg);
	}

	public static void d(String tag, String msg) {
		if (m_Enabled) Log.config(tag + " | Debug: " + msg);
	}

	public static void v(String tag, String msg) {
		if (m_Enabled) Log.fine(tag + " | Verbose: " + msg);
	}

	public static void setLevel(Level level) {
		m_Level = level;
        ch.setLevel(m_Level);
	}

	public static void enable() {
		m_Enabled = true;
	}

	public static void disable() {
		m_Enabled = false;
	}

	public static boolean isEnabled() {
		return m_Enabled;
	}
}